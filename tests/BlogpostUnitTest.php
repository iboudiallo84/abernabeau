<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Category;
use App\Entity\Peinture;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class BlogpostUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();
        $date = new \DateTime('now');
        $blogPost = (new Blogpost())
            ->setTitre('titre')
            ->setContenu('a content')
            ->setCreatedAt($date)
            ->setUser($user);

        $this->assertTrue($blogPost->getTitre() === 'titre');
        $this->assertTrue($blogPost->getUser() === $user);
        $this->assertTrue($blogPost->getCreatedAt() === $date);
        $this->assertTrue($blogPost->getContenu() === 'a content');
    }
}
