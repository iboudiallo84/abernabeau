<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = (new User())
            ->setEmail('email@email.com')
            ->setPrenom('ibou')
            ->setNom('diallo')
            ->setPassword('password')
            ->setAPropos('a-propos')
            ->setInstagram('insta');

        $this->assertTrue($user->getEmail() === 'email@email.com');
        $this->assertTrue($user->getPrenom() === 'ibou');
        $this->assertTrue($user->getNom() === 'diallo');
        $this->assertSame($user->getPassword(), 'password');
        $this->assertSame($user->getAPropos(), 'a-propos');
        $this->assertSame($user->getInstagram(), 'insta');
    }

    public function testIsFalse(): void
    {
        $user = (new User())
            ->setEmail('email@email.com')
            ->setPrenom('ibou')
            ->setNom('diallo')
            ->setPassword('password')
            ->setAPropos('a-propos')
            ->setInstagram('insta');

        $this->assertTrue($user->getEmail() === 'email@email.com');
        $this->assertTrue($user->getPrenom() === 'ibou');
        $this->assertTrue($user->getNom() === 'diallo');
        $this->assertNotSame($user->getPassword(), 'passwords');
        $this->assertNotSame($user->getAPropos(), 'a-proposs');
        $this->assertNotSame($user->getInstagram(), 'instas');
    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAPropos());
        $this->assertEmpty($user->getInstagram());
    }
}
