<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $peinture = new Peinture();
        $blogPost = new Blogpost();
        $date = new \DateTime('now');
        $commentaire = (new Commentaire())
            ->setAuteur('ibou')
            ->setEmail('email@email.com')
            ->setContenu('content')
            ->setCreatedAt($date)
            ->setPeinture($peinture)
            ->setBlogpost($blogPost);

        $this->assertTrue($commentaire->getAuteur() === 'ibou');
        $this->assertTrue($commentaire->getEmail() === 'email@email.com');
        $this->assertTrue($commentaire->getContenu() === 'content');
        $this->assertTrue($peinture === $commentaire->getPeinture());
        $this->assertTrue($blogPost === $commentaire->getBlogpost());
    }
}
