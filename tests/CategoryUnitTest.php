<?php

namespace App\Tests;

use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class CategoryUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $category = (new Category())
            ->setNom('categorieNom')
            ->setDescription('une desc')
            ->setSlug('cat-test-slug');

        $this->assertTrue($category->getNom() === 'categorieNom');
        $this->assertTrue($category->getDescription() === 'une desc');
        $this->assertTrue($category->getSlug() === 'cat-test-slug');
    }

    public function testIsFalse(): void
    {
        $category = (new Category())
            ->setNom('categorieNom')
            ->setDescription('une desc')
            ->setSlug('cat-test-slug');

        $this->assertFalse($category->getNom() === 'false');
        $this->assertFalse($category->getDescription() === 'une false');
        $this->assertFalse($category->getSlug() === 'cat-test-slug-false');
    }

    public function testIsEmpty(): void
    {
        $category = (new Category());

        $this->assertEmpty($category->getNom());
        $this->assertEmpty($category->getDescription());
        $this->assertEmpty($category->getSlug());
    }
}
