<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\Peinture;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class PeintureUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();
        $categorie = new Category();
        $peinture = (new Peinture())
            ->setNom('pname')
            ->setPortfolio(true)
            ->setLargeur(20.20)
            ->setHauteur(20.20)
            ->setEnVente(false)
            ->setPrix(3200)
            ->setUser($user)
            ->addCategorie($categorie);

        $this->assertTrue($peinture->getNom() === 'pname');
        $this->assertTrue($peinture->getPortfolio());
        $this->assertTrue($peinture->getLargeur() == 20.2);
        $this->assertTrue($peinture->getHauteur() == 20.2);
        $this->assertFalse($peinture->getEnVente());
        $this->assertTrue($peinture->getPrix() == 3200);
        $this->assertTrue($user === $peinture->getUser());
        $this->assertContains($categorie, $peinture->getCategories());
    }
}
